/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

var CONTAINER_STATE_WAITING = 1;
var CONTAINER_STATE_IN_TRANSIT = 2;
var CONTAINER_STATE_DELIVERED = 3;


function ShippingContainer(initialCell) {

	this.initialCell = initialCell;
    this.position = initialCell.pos.clone();
    this.targetPosition = new Vec2();
    this.state = CONTAINER_STATE_WAITING;
    
    
    this.render = function(context) {
        var posx = this.position.x - (gfxShippingContainer.width / 2);
        var posy = this.position.y - (gfxShippingContainer.height / 2);
        context.drawImage(gfxShippingContainer, posx, posy);
		
		if (gfxDrawTransitArrows) {
			context.strokeStyle = '#A93';
			context.beginPath();
			context.moveTo(this.position.x, this.position.y);
			context.lineTo(this.targetPosition.x, this.targetPosition.y);
			context.stroke();
		}
    }
    
	
    this.setTargetPosition = function(targetCell) {
        this.targetPosition = targetCell.pos.clone();
    }
	
	
	this.resetContainer = function() {
		this.position = this.initialCell.pos.clone();
		this.state = CONTAINER_STATE_WAITING;
	}
}
