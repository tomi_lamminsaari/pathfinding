/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */


function PathAlgorithm() {
    this.binderObject = null;
	this.containerInTransit = null;
}


PathAlgorithm.prototype.constructor = PathAlgorithm;



PathAlgorithm.prototype.setBinder = function(b) {
    this.binderObject = b;
}


PathAlgorithm.prototype.findRoute = function(fromCell, toCell) {
	return null;
}

PathAlgorithm.prototype.getRouteToPickupPoint = function() {
    return null;
}


PathAlgorithm.prototype.getRouteToDropPoint = function() {
    return null;
}


PathAlgorithm.prototype.setContainers = function(containers) {
    this.containers = containers;
}


PathAlgorithm.prototype.getWaitingContainers = function() {
    return this.binderObject.map.getWaitingShippingContainers();
}


PathAlgorithm.prototype.getCurrentPosition = function() {
    return this.binderObject.forkliftObject.position;
}

PathAlgorithm.prototype.reserveNextWaitingContainer = function() {
	var containerArray = this.getWaitingContainers();
	this.containerInTransit = containerArray[0];
	return this.containerInTransit;
}

