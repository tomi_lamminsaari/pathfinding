/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */


var canvasElem;
var ctx;
var map;
var mapWidth = 1028;
var mapHeight = 600;
var timeInterval = 0.16;

var pathFinderCreator = null;
var timerObject = null;

var mouseDown = false;
var resetted = false;


var draggedObject = null;

var CLICKMODE_NORMAL = 0;
var CLICKMODE_CARGO_TARGET = 1;
var CLICKMODE_CARGO_DELETE = 2;
var CLICKMODE_FORKLIFT_DELETE = 3;
var CLICKMODE_CARGO_POS = 4;
var mouseClickMode = 0;


function resetSimulation() {
	if (timerObject != null) {
		// Reset doesn't work when simulation is on.
		return;
	}
	map.resetMap();
	map.render(ctx);
	
	if (timerObject == null) {
		var resumeButtonElem = document.getElementById('pauseButton');
		resumeButtonElem.innerHTML = "Start";
	}
	showMessageText("");
	resetted = true;
}


function pauseResumeSimulation(elem) {
	if (timerObject == null) {
		if (resetted == true) {
		//	map.resetMap();
			map.startExecution();
		}
		timerObject = setInterval(function(){updateView()}, 30);
		elem.innerHTML = "Pause";
		document.getElementById('resetButton').innerHTML = " ";
		gfxDrawTransitArrows = false;
	} else {
		clearInterval(timerObject);
		timerObject = null;
		elem.innerHTML = "Resume";
		document.getElementById('resetButton').innerHTML = "Reset";
		gfxDrawTransitArrows = true;
	}
	map.render(ctx);
	resetted = false;
}

function addForkliftClicked(elem) {
	if (resetted == false) {
		showMessageText("You must pause and reset the simulation before you can add cars.");
	} else {
		var freeSlot = map.findFreeSlot();
		if (freeSlot != null) {
			map.spawnForklift(freeSlot);
		}
		map.render(ctx);
		showMessageText("You can move the cars by dragging them.");
	}
}

function delForkliftClicked(elem) {
	if (resetted == false) {
		showMessageText("You must pause and reset the simulation before you can add containers.");
	} else {
		mouseClickMode = CLICKMODE_FORKLIFT_DELETE;
		showMessageText("Click cargo container to delete it.");
	}
}

function addContainerClicked(elem) {
	if (resetted == false) {
		showMessageText("You must pause and reset the simulation before you can add containers.");
	} else {
		var freeSlot = map.findFreeSlot();
		if (freeSlot != null) {
			map.spawnShippingContainer(freeSlot, freeSlot);
		}
		map.render(ctx);
		showMessageText("You can move the cargo containers by dragging them.");
	}

}

function delContainerClicked(elem) {
	if (resetted == false) {
		showMessageText("You must pause and reset the simulation before you can add containers.");
	} else {
		mouseClickMode = CLICKMODE_CARGO_DELETE;
		showMessageText("Click cargo container to delete it.");
	}
}


function showMessageText(text) {
	document.getElementById('messageBox').innerHTML = text;
}


function handleMouseDown(event) {
	if (resetted == false) {
		showMessageText("You must pause and reset the map to make changes");
		return;
	}
	
	var x = event.clientX;
	var y = event.clientY;
	x -= canvasElem.offsetLeft;
	y -= canvasElem.offsetTop;
	if (mouseClickMode == CLICKMODE_CARGO_TARGET) {
		mouseClickMode = CLICKMODE_NORMAL;
		draggedObject = null;
		gfxDrawObstacles = false;
		
	} else if (mouseClickMode == CLICKMODE_CARGO_DELETE) {
		var c = map.getShippingContainerAtCoordinate(x, y);
		map.deleteContainer(c);
		mouseClickMode = CLICKMODE_NORMAL;
		
	} else if (mouseClickMode == CLICKMODE_FORKLIFT_DELETE) {
		var c = map.getForkliftAtCoordinate(x, y);
		map.deleteForklift(c);
		mouseClickMode = CLICKMODE_NORMAL;
		
	} else if (mouseClickMode == CLICKMODE_NORMAL) {
		draggedObject = map.getForkliftAtCoordinate(x, y);
		if (draggedObject != null) {
			showMessageText("Drag to move the forklift");
			
		} else {
			draggedObject = map.getShippingContainerAtCoordinate(x, y);
			if (draggedObject != null) {
				showMessageText("Drag to move the cargo container");
				mouseClickMode = CLICKMODE_CARGO_POS;
			} else {
				showMessageText("");
			}
		}
		
	} else if (mouseClickMode == CLICKMODE_CARGO_POS) {
		mouseClickMode = CLICKMODE_NORMAL;
		
	}
	map.render(ctx);
	mouseDown = true;
}

function handleMouseMove(event) {
	if (mouseDown == false) {
		return;
	}
	
	var x = event.clientX;
	var y = event.clientY;
	x -= canvasElem.offsetLeft;
	y -= canvasElem.offsetTop;
	
	if (draggedObject != null) {
		if (draggedObject instanceof ShippingContainer) {
			if (mouseClickMode == CLICKMODE_CARGO_TARGET) {
				var cell = map.getCell(newVec2(x, y));
				draggedObject.targetPosition = cell.pos.clone();
				
			} else {
				var cell = map.getCell(newVec2(x, y));
				draggedObject.initialCell = cell;
				draggedObject.position = cell.pos.clone();
			}
			
		} else if (draggedObject instanceof Forklift) {
			var cell = map.getCell(newVec2(x, y));
			draggedObject.depotCell = cell;
			draggedObject.position = cell.pos.clone();
			draggedObject.oldPosition = cell.pos.clone();
			
		}
		map.render(ctx);
	}
}

function handleMouseUp(event) {
	if (mouseDown == false) {
		return;
	}
	if (mouseClickMode == CLICKMODE_CARGO_POS) {
		return;
	}
	
	if (draggedObject != null) {
		if (draggedObject instanceof ShippingContainer) {
			showMessageText("Click the destination position");
			mouseClickMode = CLICKMODE_CARGO_TARGET;
			gfxDrawObstacles = true;
			return;
		}
		
	}
	
	showMessageText("");
	
	cargoTargetSelectionMode = false;
	draggedObject = null;
	mouseDown = false;
}


function pageLoaded() {
    canvasElem = document.getElementById('simulationCanvas');
    ctx = canvasElem.getContext('2d');
    
	canvasElem.addEventListener('mousedown', handleMouseDown, true);
	canvasElem.addEventListener('mousemove', handleMouseMove, true);
	canvasElem.addEventListener('mouseup', handleMouseUp, true);
	
    loadAllGraphics();
    
    map = new Map(mapWidth, mapHeight);
    pathFinderCreator = bruteSearchCreator;
        
    // Create shipping containers.
	
/*	for (var i=0; i < 6; ++i) {
		var mapcell = map.getCell(newVec2(128, 338 + i*20));
		var targetCell = map.getCell(newVec2(670 + i*20, 240));
		map.spawnShippingContainer(mapcell, targetCell);
	}
	
	for (var i=0; i < 6; ++i) {
		var mapcell = map.getCell(newVec2(112, 338 + i*20));
		var targetCell = map.getCell(newVec2(320 + i*20, 225));
		map.spawnShippingContainer(mapcell, targetCell);
	}*/
    
	for (var i=0; i < 8; ++i) {
		var mapcell = map.getCell(newVec2(200 + i*20, 481));
		var targetCell = map.getCell(newVec2(450 + i*20, 75));
		map.spawnShippingContainer(mapcell, targetCell);
		
		mapcell = map.getCell(newVec2(720 + i*20, 480 - i*20));
		targetCell = map.getCell(newVec2(450 + i*20, 60));
		map.spawnShippingContainer(mapcell, targetCell);
	}
	for (var i=0; i < 10; ++i)
	{
		var mapcell = map.getCell(newVec2(120, 320 + i*20));
		var targetcell = map.getCell(newVec2(700, 120 + i*20));
		map.spawnShippingContainer(mapcell, targetcell);
	}
/*	for (var i=0; i < 8; ++i) {
		var mapcell = map.getCell(newVec2(720 + i*20, 480 - i*20));
		var targetCell = map.getCell(newVec2(450 + i*20, 60));
		map.spawnShippingContainer(mapcell, targetCell);
	}*/
	
	
    // Create forklifts
	depotCell = map.getCell(newVec2(720, 50));
    map.spawnForklift(depotCell);
	
	depotCell = map.getCell(newVec2(720, 100));
	map.spawnForklift(depotCell);
	
	depotCell = map.getCell(newVec2(720, 150));
	map.spawnForklift(depotCell);
    
	depotCell = map.getCell(newVec2(750, 40));
	map.spawnForklift(depotCell);
	
	
    map.startExecution();
    timerObject = setInterval(function(){updateView()}, 30);
}

function checkLineIntersection(l1p1, l1p2, l2p1, l2p2) {
	var denominator = ((l1p2.x - l1p1.x)*(l2p2.y - l2p1.y)) - ((l1p2.y - l1p1.y)*(l2p2.x - l2p1.x));
	var num1 = ((l1p1.y - l2p1.y)*(l2p2.x - l2p1.x)) - ((l1p1.x - l2p1.x)*(l2p2.y - l2p1.y));
	var num2 = ((l1p1.y - l2p1.y)*(l1p2.x - l1p1.x)) - ((l1p1.x - l2p1.x)*(l1p2.y - l1p1.y));
	if (Math.abs(denominator) < 0.00001) {
		if (Math.abs(num1) < 0.00001 && Math.abs(num2) < 0.00001) {
			return true;
		}
		return false;
	}
	
	var r = num1 / denominator;
	var s = num2 / denominator;
	
	return (r >= 0.0 && r <= 1.0) && (s >= 0 && s <= 1.0);
}


function render() {
    map.render(ctx);
}

function updateView() {
    map.update(timeInterval);
    render();
}

