/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

var CANVAS_WIDTH = 1028;
var CANVAS_HEIGHT = 600;

var gfxDrawTransitArrows = false;
var gfxDrawObstacles = false;

var gfxShippingContainer = null;
var gfxForklift = null;
var gfxExplosion = null;

function loadAllGraphics() {
    gfxShippingContainer = new Image();
    gfxShippingContainer.src = 'img/shipping_container.png';
    
    gfxForklift = new Image();
    gfxForklift.src = 'img/forklift.png';
	
	gfxExplosion = new Image();
	gfxExplosion.src = 'img/explosion.png';
}



