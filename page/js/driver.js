/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */
 
var WAITING_DELAY_PICKUP = 10;
var WAITING_DELAY_DROP = 50;
var WAITING_DELAY_ROUTE = 85;

var DRIVER_ACTIVITY_IDLE = 0;
var DRIVER_ACTIVITY_PICKUP_DELAY = 1;
var DRIVER_ACTIVITY_DROPDOWN_DELAY = 2;
var DRIVER_ACTIVITY_IN_TRANSIT_PICKUP = 3;
var DRIVER_ACTIVITY_IN_TRANSIT_DROPDOWN = 4;
var DRIVER_ACTIVITY_IN_TRANSIT_DEPOT = 5;
var DRIVER_ACTIVITY_WAITING_ROUTE = 6;


function Driver() {
	this.name = "";
    this.binder = null;
    this.route = null;
    this.currentCellIndex = 0;
    this.targetCell = null;
    this.velocity = 10;
    this.isStarted = false;
    this.waitingDelay = 0;
    this.activityType = DRIVER_ACTIVITY_IDLE;
	this.reservedContainer = null;
	this.previousActivityType = DRIVER_ACTIVITY_IDLE;
	this.ourCells = new Array();
}


Driver.prototype.constructor = Driver;


Driver.prototype.resetDriver = function() {
	this.route = null;
	this.currentCellIndex = 0;
	this.targetCell = null;
	this.isStarted = false;
	this.waitingDelay = 0;
	this.activityType = DRIVER_ACTIVITY_IDLE;
	this.reservedContainer = null;
	this.ourCells = new Array();
}


Driver.prototype.isInMotion = function() {
	var inMotion = false;
	switch (this.activityType) {
		case DRIVER_ACTIVITY_IN_TRANSIT_PICKUP:
		case DRIVER_ACTIVITY_IN_TRANSIT_DROPDOWN:
		case DRIVER_ACTIVITY_IN_TRANSIT_DEPOT:
			if (this.route != null) {
				inMotion = true;
			}
			break;
		default:
			inMotion = false;
			break;
	}
	return inMotion;
}


Driver.prototype.getCurrentRouteLineSegment = function() {
	if (this.isInMotion() == false || this.currentCellIndex - 1 < 0) {
		return null;
	}
	
	var c1 = this.route[this.currentCellIndex - 1];
	var c2 = this.route[this.currentCellIndex];
	var arr = new Array();
	arr.push(c1.pos);
	arr.push(c2.pos);
	return arr;
}


Driver.prototype.setBinder = function(b) {
    this.binder = b;
}


Driver.prototype.start = function(route) {
    // Start by finding route to loading point.
    console.log('--> Driver.prototype.start()');
	this.waitingDelayEnded();
    this.isStarted = true;
    console.log('<-- Driver.prototype.start()');
}


Driver.prototype.goToDepot = function() {
	var toCell = this.binder.forkliftObject.depotCell;
	var fromCell = this.binder.map.getCell(this.binder.forkliftObject.position);
	this.route = this.binder.algorithmObject.findRoute(fromCell, toCell);
	if (this.route != null) {
		this.targetCell = this.route[1];
		this.currentCellIndex = 0;
		this.activityType = DRIVER_ACTIVITY_IN_TRANSIT_DEPOT;
	}
}


Driver.prototype.waitingDelayEnded = function() {
	console.log('wait ended');
	var map = this.binder.map;
    if (this.activityType == DRIVER_ACTIVITY_PICKUP_DELAY) {
		
		if (this.reservedContainer != null) {
			
			// Find route from current (pick up) position to drop down position.
			console.log('Find route to drop position');
			var fromCell = map.getCell(this.binder.forkliftObject.position);
			var toCell = map.getCell(this.reservedContainer.targetPosition);
			this.route = this.binder.algorithmObject.findRoute(fromCell, toCell);
			if (this.route != null) {
				this.targetCell = this.route[1];
				this.currentCellIndex = 0;
				this.activityType = DRIVER_ACTIVITY_IN_TRANSIT_DROPDOWN;
				
				// Start carrying the container.
				this.binder.forkliftObject.pickedUpContainer = this.reservedContainer;
				this.reservedContainer = null;
			} else {
				// Target cell is locked. Wait until it's freed.
				this.waitingDelay = WAITING_DELAY_ROUTE;
			}
		}
        
    } else if (this.activityType == DRIVER_ACTIVITY_DROPDOWN_DELAY || this.activityType == DRIVER_ACTIVITY_IDLE) {

		console.log('Reserve container');
		
		this.reservedContainer = this.binder.algorithmObject.reserveNextWaitingContainer();
		if (this.reservedContainer == null) {
			console.log('No containers to move. Going to depot!');
			this.goToDepot();
			
		} else {
			// Find route from current position to next container.
			var fromCell = map.getCell(this.binder.forkliftObject.position);
			var toCell = map.getCell(this.reservedContainer.position);
			this.route = this.binder.algorithmObject.findRoute(fromCell, toCell);
			if (this.route != null) {
				this.targetCell = this.route[1];
				this.currentCellIndex = 0;
				this.activityType = DRIVER_ACTIVITY_IN_TRANSIT_PICKUP;
				
				// Mark this container that it is not waiting anymore.
				this.reservedContainer.state = CONTAINER_STATE_IN_TRANSIT;
			} else {
				// Target cell is locked. Wait until it's freed.
				this.waitingDelay = WAITING_DELAY_ROUTE;
			}
			
		}
    } else if (this.activityType == DRIVER_ACTIVITY_WAITING_ROUTE) {
		this.activityType = this.previousActivityType;
		console.log('continue path');
		
	}
}


Driver.prototype.releaseRouteCells = function(route) {
	if (route != null) {
		for (var i=0; i < route.length; ++i) {
			if (route[i].reservedBy === this) {
				route[i].reservedBy = null;
			}
		}
	}
}


Driver.prototype.allocateRouteCells = function(route, currentIndex) {
	var failedCell = null;
	if (route != null) {
		if (currentIndex < route.length) {
			if (route[currentIndex].reservedBy != null) {
				failedCell = route[currentIndex];
			} else {
				route[currentIndex].reservedBy = this;
			}
		}
		
		if (currentIndex+1 < route.length) {
			if (route[currentIndex+1].reservedBy != null) {
				failedCell = route[currentIndex+1];
			} else {
				route[currentIndex+1].reservedBy = this;
			}
		}
		
		if (currentIndex+2 < route.length) {
			if (route[currentIndex+2].reservedBy != null) {
				failedCell = route[currentIndex+2];
			} else {
				route[currentIndex+2].reservedBy = this;
			}
		}
		
		if (currentIndex-1 >= 0) {
			if (route[currentIndex-1].reservedBy != null) {
				failedCell = route[currentIndex-1];
			} else {
				route[currentIndex-1].reservedBy = this;
			}
		}
	}
	return failedCell;
}

Driver.prototype.releaseCurrentCell = function() {
	for (var i=0; i < this.ourCells.length; ++i) {
		this.ourCells[i].reservedBy = null;
	}
	this.ourCells = null;
	this.ourCells = new Array();
}


Driver.prototype.reserveCurrentCell = function() {
	var cell = this.binder.map.getCell(this.binder.forkliftObject.position);
	if (cell.reservedBy != null) {
		if (cell.reservedBy != this) {
			console.log('BIG MISTAKE! Two forklifts in same cell: ', this.binder.forkliftObject.position);
		}
	} else {
		cell.reservedBy = this;
		this.ourCells.push(cell);
	}
}


Driver.prototype.reserveMapCells = function() {
	this.releaseRouteCells(this.route);
	var failedCell = this.allocateRouteCells(this.route, this.currentCellIndex);
	if (failedCell == null) {
/*		if (this.binder.map.checkPathIntersections(this) != null) {
			var fromCell = this.binder.map.getCell(this.binder.forkliftObject.position);
			var toCell = this.route[this.route.length - 1];
			this.targetCell.obstacle = 1;
			this.releaseRouteCells(this.route);
			this.route = this.binder.algorithmObject.findRoute(fromCell, toCell);
			if (this.route != null) {
				this.targetCell = this.route[1];
				this.currentCellIndex = 0;
			}
			return;
		}*/
	}
	if (failedCell != null) {
		
		var ourDir = this.binder.forkliftObject.getLatestDirection();
		var otherDir = failedCell.reservedBy.binder.forkliftObject.getLatestDirection();
		if (vec2DotProduct(ourDir, otherDir) < 0.0) {
			// Forklifts are going opposite directions. Find new route to avoid
			// collision.
			var fromCell = this.binder.map.getCell(this.binder.forkliftObject.position);
			var toCell = this.route[this.route.length-1];
			this.releaseRouteCells(this.route);
			
			this.route = this.binder.algorithmObject.findRoute(fromCell, toCell);
			if (this.route != null) {
				this.targetCell = this.route[1];
				this.currentCellIndex = 0;
			}
			
		} else {
			// Forklifts are going pretty much to the same direction. Just wait until
			// the other goes away.
			this.previousActivityType = this.activityType;
			this.activityType = DRIVER_ACTIVITY_WAITING_ROUTE;
			this.waitingDelay = WAITING_DELAY_ROUTE;
			this.releaseRouteCells(this.route);
		}
		
	}
}


Driver.prototype.update = function() {
    if (this.isStarted == false) {
        return;
    }
    
    
    if (this.waitingDelay > 0) {
        this.waitingDelay -= 1;
        if (this.waitingDelay <= 0) {
            this.waitingDelayEnded();
        }
        
    } else if (this.targetCell != null) {
    
        var fl = this.binder.forkliftObject;
        var dir = vec2Sub(this.targetCell.pos, fl.position);
        dir.normalize();
        dir.scale(this.velocity * timeInterval);
        fl.move(dir);
		
		this.reserveMapCells();
        
        dir = vec2Sub(this.targetCell.pos, fl.position);
        if (dir.getLength() < 3.0) {
            this.currentCellIndex += 1;
            if (this.currentCellIndex >= this.route.length) {
				this.releaseRouteCells(this.route);
                console.log('You have reached your destination!');
                if (this.activityType == DRIVER_ACTIVITY_IN_TRANSIT_PICKUP) {
                    this.activityType = DRIVER_ACTIVITY_PICKUP_DELAY;
                    this.waitingDelay = WAITING_DELAY_PICKUP;
					console.log('Picking up');
					
                } else if (this.activityType == DRIVER_ACTIVITY_IN_TRANSIT_DROPDOWN) {
                    this.activityType = DRIVER_ACTIVITY_DROPDOWN_DELAY;
                    this.waitingDelay = WAITING_DELAY_DROP;
					this.binder.forkliftObject.pickedUpContainer = null; // drop container
					
                } else if (this.activityType == DRIVER_ACTIVITY_IN_TRANSIT_DEPOT) {
					this.activityType = DRIVER_ACTIVITY_IDLE;
					this.isStarted = false;
					
				}
                
            } else {
                this.targetCell = this.route[this.currentCellIndex];
            }
        }
    }
}



