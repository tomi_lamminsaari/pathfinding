/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */


function Vec2() {
    this.x = 0.0;
    this.y = 0.0;
    
    this.setXY = function(posX, posY) {
        this.x = posX;
        this.y = posY;
    }
    
    this.getLength = function() {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }
    
    this.add = function(anotherVec) {
        this.x += anotherVec.x;
        this.y += anotherVec.y;
    }
    
    this.sub = function(anotherVec) {
        this.x -= anotherVec.x;
        this.y -= anotherVec.y;
    }
    
    this.scale = function(factor) {
        this.x *= factor;
        this.y *= factor;
    }
    
    this.normalize = function() {
        var l = this.getLength();
        this.x /= l;
        this.y /= l;
    }
    
    this.rotateDegs = function(degrees) {
        var rads = degrees * (3.14159 / 180.0);
        var sinD = Math.sin(rads);
        var cosD = Math.cos(rads);
        var newX = (this.x * cosD) - (this.y * sinD);
        var newY = (this.x * sinD) + (this.y * cosD);
        this.x = newX;
        this.y = newY;
    }
    
    this.rotateRads = function(radians) {
        var sinD = Math.sin(radians);
        var cosD = Math.cos(radians);
        this.x = (this.x * cosD) - (this.y * sinD);
        this.y = (this.x * sinD) + (this.y * cosD);
    }
    
    this.clone = function() {
        var v = new Vec2();
        v.setXY(this.x, this.y);
        return v;
    }
}


function newVec2(posX, posY) {
    var v = new Vec2();
    v.setXY(posX, posY);
    return v;
}

function vec2Add(v1, v2) {
    var vecOut = new Vec2();
    vecOut.x = v1.x + v2.x;
    vecOut.y = v1.y + v2.y;
    return vecOut;
}

function vec2Sub(v1, v2) {
    var vecOut = new Vec2();
    vecOut.x = v1.x - v2.x;
    vecOut.y = v1.y - v2.y;
    return vecOut;
}

function vec2DotProduct(v1, v2) {
	return (v1.x * v2.x) + (v1.y * v2.y);
}


