/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */


function LogicBinder() {
    this.driverObject = null;
    this.algorithmObject = null;
    this.forkliftObject = null;
    this.map = null;
}

LogicBinder.prototype.constructor = LogicBinder;


LogicBinder.prototype.setMap = function(map) {
    this.map = map;
}

LogicBinder.prototype.resetObjects = function() {
	this.driverObject.resetDriver();
	this.forkliftObject.resetForklift();
}


