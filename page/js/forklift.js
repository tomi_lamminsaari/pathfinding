/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

function Forklift(depotCell) {

    this.position = depotCell.pos.clone();
    this.oldPosition = depotCell.pos.clone();
	this.depotCell = depotCell;
	this.pickedUpContainer = null;
	
	
    this.move = function(dirVec) {
        this.oldPosition = this.position.clone();
        this.position.add(dirVec);
		if (this.pickedUpContainer != null) {
			this.pickedUpContainer.position.add(dirVec);
		}
    }
    
	
	this.getLatestDirection = function() {
		return vec2Sub(this.position, this.oldPosition);
	}
    
    this.render = function(context) {
        var posx = this.position.x - (gfxForklift.width / 2);
        var posy = this.position.y - (gfxForklift.height / 2);
        context.drawImage(gfxForklift, posx, posy);
    }
    
	
	this.resetForklift = function() {
		this.position = this.depotCell.pos.clone();
		this.oldPosition = this.depotCell.pos.clone();
		this.pickedUpContainer = null;
	}
}
