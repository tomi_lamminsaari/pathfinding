/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

function Obstacle(centerPos, width, height, rotationDegs) {
    this.centerPos = centerPos;
    this.w = width;
    this.h = height;
    this.rot = rotationDegs;
	this.isHidden = false;
    
    this.render = function(context) {
		if (this.isHidden) {
			return;
		}
        var pos = this.getCorners();
        
        context.fillStyle = '#f00';
        context.beginPath();
        context.moveTo(pos[0].x, pos[0].y);
        context.lineTo(pos[1].x, pos[1].y);
        context.lineTo(pos[2].x, pos[2].y);
        context.lineTo(pos[3].x, pos[3].y);
        context.lineTo(pos[0].x, pos[0].y);
        context.fill();
    }
    
    this.getCorners = function() {
        var tl = newVec2(-(this.w / 2.0), -(this.h / 2.0));
        tl.rotateDegs(this.rot);
        var bl = newVec2(-(this.w / 2.0), (this.h / 2.0));
        bl.rotateDegs(this.rot);
        var tr = newVec2(this.w / 2.0, -(this.h / 2.0));
        tr.rotateDegs(this.rot);
        var br = newVec2(this.w / 2.0, this.h / 2.0);
        br.rotateDegs(this.rot);
        
        tl.add(this.centerPos);
        bl.add(this.centerPos);
        tr.add(this.centerPos);
        br.add(this.centerPos);
        
        var arr = new Array();
        arr.push(tl);
        arr.push(tr);
        arr.push(br);
        arr.push(bl);
        return arr;
    }
}
