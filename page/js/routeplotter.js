/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

function RoutePlotter() {

	this.render = function(context, route) {
		if (route != null) {
			context.strokeStyle = '#457';
			if (route.length > 1) {
				context.beginPath();
				context.moveTo(route[0].pos.x, route[0].pos.y);
				for (var i=1; i < route.length; ++i) {
					context.lineTo(route[i].pos.x, route[i].pos.y);
				}
				context.stroke();
			}
		}
	}
	
}
