/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

function BruteSearch() {
}

BruteSearch.prototype = new PathAlgorithm();
BruteSearch.prototype.constructor = BruteSearch;


BruteSearch.prototype.findRoute = function(fromCell, toCell) {
	return this.findPath(fromCell, toCell);
}


BruteSearch.prototype.getRouteToPickupPoint = function() {
    var tmpContainers = this.getWaitingContainers();
    if (tmpContainers.length <= 0) {
        return null;
    }
    
    console.log('Containers in waiting-state: %d', tmpContainers.length);
    
	// Start position is forklifts current position and
	// destination point is the first waiting shipping container.
    var startP = this.getCurrentPosition();
    var endP = tmpContainers[0].position;
    
	// Convert positions from coordinates to cell positions.
    var map = this.binderObject.map;
    var startCell = map.getCell(startP);
    var endCell = map.getCell(endP);
    
    return this.findPath(startCell, endCell);
}


BruteSearch.prototype.getRouteToDropPoint = function() {
	
	var tmpContainers = this.getWaitingContainers();
	if (tmpContainers.length <= 0) {
		return null;
	}
	
	// Starting point is forklifts current position and
	// destination point is the target position of the
	// shipping container.
	var startP = this.getCurrentPosition();
	var endP = tmpContainers[0].targetPosition;
	
	// Convert from coordinates to cell positions.
	var map = this.binderObject.map;
	var startCell = map.getCell(startP);
	var endCell = map.getCell(endP);
	
	return this.findPath(startCell, endCell);
}


BruteSearch.prototype.findPath = function(p1, p2) {
    var targetCell = p2;
    
	var algo = new AStarAlgorithm();
	algo.setMap(this.binderObject.map);
	var routeCells = algo.findPath(p1, p2);
    return routeCells;
}

function bruteSearchCreator() {
    var a = new BruteSearch();
    return a;
}
