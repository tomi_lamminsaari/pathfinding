/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

function AStarAlgorithm() {
    this.map = null;
    this.pathNodes = new Array();
}

AStarAlgorithm.prototype.constructor = AStarAlgorithm;

AStarAlgorithm.prototype.setMap = function(map) {
    this.map = map;
}

AStarAlgorithm.prototype.findPath = function(fromCell, toCell) {
	console.log('--> AStarAlgorithm.findPath(): ', fromCell.pos, toCell.pos);
	if (fromCell == null || toCell == null) {
		console.log('PathFind: fromCell or toCell is missing');
		return null;
	}
	this.map.clearPathfindingValues();
	
    var closedSet = new Array();
    var openSet = new Array();
    openSet.push(fromCell);
    
    var nodeTravelCost = 0;
    var nodeGoodness = fromCell.getDistanceFromCell(toCell);
    var nodeTotalCost = nodeTravelCost + nodeGoodness;
    var nodeParent = null;
    var pathFound = false;
	
    while (openSet.length > 0) {
        var currentNode = openSet[0];
        if (currentNode === toCell) {
            // We have reached the end.
			pathFound = true;
            break;
        }
		
        openSet.splice(0, 1);
        closedSet.push(currentNode);
		
        var neighbors = this.map.getAllAdjacentCells(currentNode, fromCell);
        for (var i=0; i < neighbors.length; ++i) {
            var neighbor = neighbors[i];
            nodeTravelCost = currentNode.getDistanceFromCell(neighbor);
            nodeGoodness = 1.05 * neighbor.getDistanceFromCell(toCell);
            nodeTotalCost = nodeTravelCost + nodeGoodness;
            nodeParent = currentNode;
            
            var foundIndex = -1;
            for (var a=0; a < openSet.length; ++a) {
                if (openSet[a].index == neighbor.index) {
                    foundIndex = a;
                    break;
                }
            }
            if (foundIndex != -1) {
                if (nodeTravelCost <= openSet[foundIndex].travelCost) {
                    openSet[foundIndex].pathTravelCost = nodeTravelCost;
                    openSet[foundIndex].pathGoodness = nodeGoodness;
					openSet[foundIndex].pathTotalCost = nodeTravelCost + nodeGoodness;
					openSet[foundIndex].pathParent = current;
                }
            }
            
            if (foundIndex == -1) {
				for (var a=0; a < closedSet.length; ++a) {
					if (closedSet[a].index == neighbor.index) {
						foundIndex = a;
						break;
					}
				}
				if (foundIndex != -1) {
					if (nodeTravelCost <= closedSet[foundIndex].travelCost) {
						closedSet[foundIndex].travelCost = nodeTravelCost;
						closedSet[foundIndex].pathGoodness = nodeGoodness;
						closedSet[foundIndex].pathTotalCost = nodeTravelCost + nodeGoodness;
						closedSet[foundIndex].pathParent = current;
					}
				}
			}

			if (foundIndex == -1) {
				neighbor.pathTravelCost = nodeTravelCost;
				neighbor.pathGoodness = nodeGoodness;
				neighbor.pathTotalCost = nodeTravelCost + nodeGoodness;
				neighbor.pathParent = currentNode;
				openSet.push(neighbor);
			}
            
        }
		openSet.sort(mapCellSortFunction);
    }
	
	var path = null;
	if (pathFound) {
		// Retrace the route.
		path = new Array();
		var currentCell = toCell;
		do {
			path.push(currentCell);
			currentCell = currentCell.pathParent;
			if (currentCell == null) {
				console.log('    Path not found!');
				return null;
			}
		} while (currentCell != fromCell);
		path.reverse();
		console.log('    Found path length: %d', path.length);

	} else {
		console.log('    Path not found!');
	}
	console.log('<-- AStarAlgorithm.findPath()');
	return path;
}

function mapCellSortFunction(a, b) {
	if (a.pathTotalCost < b.pathTotalCost) {
		return -1;
	}
	return 1;
}
