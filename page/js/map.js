/**
 * Written by Tomi Lamminsaari in 2013.
 * Feel free to use.
 */

var bgImage;

var cellWidth = 0;
var cellHeight = 0;
var cellCountX = 60;
var cellCountY = 40;

function Cell() {
    this.pos = new Vec2();
    this.obstacle = 0;
	this.vechile = null;
	this.reservedBy = null;
    this.index = 0;
	this.connections = new Array();
    this.pathTravelCost = 0; // reserved for path algorithms
    this.pathGoodness = 0; // reserved for path algorith
	this.pathTotalCost = 0; // reserved for path algorithm
	this.pathParent = null; // reserved for path algorithm
    
    this.render = function(context) {
		if (this.obstacle != 0 && gfxDrawObstacles) {
			context.fillStyle = '#FFF';
            context.fillRect(this.pos.x-2, this.pos.y-2, 6, 6);
            context.strokeRect(this.pos.x - (cellWidth / 2), this.pos.y - (cellHeight / 2), cellWidth, cellHeight);
		}
		if (this.obstacle == 0) {
            context.fillStyle = '#0F0';
//            context.fillRect(this.pos.x-1, this.pos.y-1, 2, 2);
		}
		if (this.reservedBy != null) {
			context.fillStyle = '#F00';
			context.fillRect(this.pos.x-2, this.pos.y-2, 4, 4);
        }
//		context.fillStyle = '#FFF';
//		context.fillText(this.pathTotalCost.toFixed(2), this.pos.x, this.pos.y);
    }
    
    this.getDistanceFromCell = function(anotherCell) {
        var cy1 = Math.floor(anotherCell.index / cellCountX);
        var cx1 = anotherCell.index - (cy1 * cellCountX);
        var cy2 = Math.floor(this.index / cellCountX);
        var cx2 = this.index - (cy2 * cellCountX);
        var dx = cx2 - cx1;
        var dy = cy2 - cy1;
        return Math.sqrt(dx*dx + dy*dy);
    }
	
	this.getManhattanDistance = function(anotherCell) {
		return Math.abs(this.pos.x - anotherCell.pos.x) + Math.abs(this.pos.y - anotherCell.pos.y);
	}
	
	this.addConnection = function(toCell) {
		var c = new Edge(toCell);
		this.connections.push(toCell);
	}
	
	this.removeConnection = function(toCell) {
		for (var i=0; i < this.connections.length; ++i) {
			if (this.connections[i].toCell == toCell) {
				this.connections.splice(i, 1);
				break;
			}
		}
	}
}




function Map(canvasW, canvasH) {
    
    this.obstacles = new Array();
	this.binders = new Array();
    this.cells = new Array();
    this.containers = new Array();
    this.bgImage = new Image();
    this.bgImage.onload = render;
    this.width = canvasW;
    this.height = canvasH;
    this.bgImage.src = 'img/map_background.png';
    cellWidth = canvasW / cellCountX;
    cellHeight = canvasH / cellCountY;
	this.routePlotter = new RoutePlotter();
    
    
    
    // Create the obstacles
    this.obstacles.push(new Obstacle(newVec2(480,225), 90, 40, 65));
    this.obstacles.push(new Obstacle(newVec2(128, 90), 190, 30, 10));
    this.obstacles.push(new Obstacle(newVec2(640, 233), 25, 200, 0));
    this.obstacles.push(new Obstacle(newVec2(440, 398), 190, 50, 0));
	this.obstacles.push(new Obstacle(newVec2(336, 300), 300, 60, 0));
	this.obstacles.push(new Obstacle(newVec2(740, 380), 25, 370, 45));
	
	var ob1 = new Obstacle(newVec2(37, 454), 60, 270, 0);
	ob1.isHidden = true;
	this.obstacles.push(ob1);
	ob1 = new Obstacle(newVec2(260, 560), 520, 50, 0);
	ob1.isHidden = true;
	this.obstacles.push(ob1);
	
	//this.obstacles.push(new Obstacle(newVec2(0, 527), 513, 72, 0));
	
	
    this.render = function(context) {
        context.drawImage(this.bgImage, 0, 0);
        
        for (var i=0; i < this.obstacles.length; ++i) {
            this.obstacles[i].render(context);
        }
        
		context.font = "10px Arial";
        context.fillStyle = "#00FF00";
		context.strokeStyle = '#FFF';
        for (var i=0; i < this.cells.length; ++i) {
            this.cells[i].render(context);
        }
    
        for (var i=0; i < this.containers.length; ++i) {
            this.containers[i].render(context);
        }
        
		for (var i=0; i < this.binders.length; ++i) {
			this.routePlotter.render(context, this.binders[i].driverObject.route);
		}
        for (var i=0; i < this.binders.length; ++i) {
            var lb = this.binders[i];
            lb.forkliftObject.render(context);
        }
		
		for (var i=0; i < this.binders.length-1; ++i) {
			for (var j=i+1; j < this.binders.length; ++j) {
				var fl1 = this.binders[i].forkliftObject;
				var fl2 = this.binders[j].forkliftObject;
				var vec = vec2Sub(fl1.position, fl2.position);
				if (vec.getLength() < 10.0) {
					vec.scale(0.5);
					vec.add(fl2);
					context.drawImage(gfxExplosion, vec.x, vec.y);
				}
			}
		}
    }
    
    
    this.update = function(timeStep) {
        for (var i=0; i < this.binders.length; ++i) {
			this.binders[i].driverObject.releaseCurrentCell();
			this.binders[i].driverObject.reserveCurrentCell();
            this.binders[i].driverObject.update(timeStep);
        }
    }
    
    
    this.getCell = function(coord) {
        if (coord.x < 0 || coord.y < 0 || coord.x >= this.width || coord.y >= this.height) {
            return null;
        }
        var cx = Math.floor(coord.x / cellWidth);
        var cy = Math.floor(coord.y / cellHeight);
        return this.cells[cy * cellCountX + cx];
    }
    
    this.getCellByIndex = function(colPos, rowPos) {
        if (colPos < 0 || colPos >= cellCountX || rowPos < 0 || rowPos >= cellCountY) {
            return null;
        }
        var index = rowPos * cellCountX + colPos;
        return this.cells[index];
    }
    
    
    this.getAllAdjacentCells = function(singleCell, fromCell) {
        var adjacents = new Array();
        var origY = Math.floor(singleCell.index / cellCountX);
        var origX = singleCell.index - (origY * cellCountX);
                
        for (var x=-1; x<=1; ++x) {
            for (var y=-1; y<=1; ++y) {
                if (x == 0 && y == 0) {
                    // The given cell itself is not included.
                    continue;
                }
                var cx = origX + x;
                var cy = origY + y;
                var c = this.getCellByIndex(cx, cy);
                if (c != null) {
					if (c.obstacle != 0) {
						continue;
					}
					if (c.reservedBy != null) {
						if (fromCell.getManhattanDistance(c) < 5*cellWidth) {
							continue;
						}
					}
                    
                    adjacents.push(c);
                }
            }
        }
        return adjacents;
    }
	
	
	this.getAllAdjacentCells2 = function(singleCell) {
		var adjacents = new Array();
        var origY = Math.floor(singleCell.index / cellCountX);
        var origX = singleCell.index - (origY * cellCountX);
                
        for (var x=-1; x<=1; ++x) {
            for (var y=-1; y<=1; ++y) {
                if (x == 0 && y == 0) {
                    // The given cell itself is not included.
                    continue;
                }
                var cx = origX + x;
                var cy = origY + y;
                var c = this.getCellByIndex(cx, cy);
                if (c != null) {
                    adjacents.push(c);
                }
            }
        }
        return adjacents;
	}
    
    this.createCells = function() {
        var cellPosX = 0;
        var cellPosY = cellHeight / 2.0;
        var index = 0;
        for (var j=0; j < cellCountY; ++j) {
            cellPosX = cellWidth / 2.0;
            for (var i=0; i < cellCountX; ++i) {
                var cell = new Cell();
                cell.pos = newVec2(cellPosX, cellPosY);
                cell.index = index;
                
                this.cells[index] = cell;
                index++;
                
                cellPosX += cellWidth;
            }
            cellPosY += cellHeight;
        }
    }
    
    
    this.interpolateAndMarkObstacle = function(pos1, pos2) {
        var p = pos1.clone();
        var dir = pos2.clone();
        dir.sub(pos1);
        var l = dir.getLength();
        dir.normalize();
        
        var cell = this.getCell(p);
        if (cell != null) {
            cell.obstacle = 1;
            p.add(dir);
        }
        for (var i=0.0; i < l; i+=1.0) {
            var cell = this.getCell(p);
            cell.obstacle = 1;
            p.add(dir);
        }
    }
    
    
    this.markObstacles = function() {
        for (var i=0; i < this.obstacles.length; ++i) {
            var obs = this.obstacles[i];
            
            var corners = obs.getCorners();
            this.interpolateAndMarkObstacle(corners[0], corners[1]);
            this.interpolateAndMarkObstacle(corners[1], corners[2]);
            this.interpolateAndMarkObstacle(corners[2], corners[3]);
            this.interpolateAndMarkObstacle(corners[3], corners[0]);
        }
    }
    
    
    this.startExecution = function() {
        for (var i=0; i < this.binders.length; ++i) {
            this.binders[i].driverObject.start();
        }
    }
    
    this.spawnForklift = function(depotCell) {
//        console.log('spawn position: %f, %f', mapCell.pos.x, mapCell.pos.y);
        
        var lb = new LogicBinder();
		
        lb.forkliftObject = new Forklift(depotCell);
        lb.driverObject = new Driver();
        lb.algorithmObject = pathFinderCreator();
        lb.setMap(this);
        
        lb.driverObject.setBinder(lb);
        lb.algorithmObject.setBinder(lb);
       
        this.binders.push(lb);
    }
    
    
    this.spawnShippingContainer = function(fromCell, toCell) {
        var c = new ShippingContainer(fromCell);
        c.setTargetPosition(toCell);
        this.containers.push(c);
    }
    
    
    this.getWaitingShippingContainers = function() {
        var tmpContainers = new Array();
        for (var i=0; i < this.containers.length; ++i) {
            var con = this.containers[i];
            if (con.state == CONTAINER_STATE_WAITING) {
                tmpContainers.push(con);
            }
        }
        return tmpContainers;
    }
    
	
	this.clearPathfindingValues = function() {
		for (var i=0; i < this.cells.length; ++i) {
			this.cells[i].pathTravelCost = 0;
			this.cells[i].pathGoodness = 0;
			this.cells[i].pathTotalCost = 0;
			this.cells[i].pathParent = null;
		}
	}
	
	
	this.resetMap = function() {
		for (var i=0; i < this.containers.length; ++i) {
			this.containers[i].resetContainer();
		}
		for (var i=0; i < this.binders.length; ++i) {
			this.binders[i].resetObjects();
		}
		for (var i=0; i < this.cells.length; ++i) {
			this.cells[i].reservedBy = null;
		}
	}
	
	
	this.findFreeSlot = function() {
		for (var y=2; y < cellCountY-2; ++y) {
			for (var x=2; x < cellCountX-2; ++x) {
				var cell = this.getCellByIndex(x, y);
				
				// Is there an obstacle in the cell.
				if (cell.obstacle != 0) {
					continue;
				}
				
				// Is there another container in the cell.
				var occupiedCell = false;
				for (var i=0; i < this.containers.length; ++i) {
					if (this.containers[i].initialCell === cell) {
						occupiedCell = true;
						break;
					}
				}
				if (occupiedCell) {
					continue;
				}
				
				// Is there a forklift in the cell.
				for (var i=0; i < this.binders.length; ++i) {
					if (this.binders[i].forkliftObject.depotCell === cell) {
						occupiedCell = true;
						break;
					}
				}
				if (occupiedCell) {
					continue;
				}
				
				// This is a free cell.
				return cell;
			}
		}
		return null;
	}
	
	
	this.getForkliftAtCoordinate = function(posX, posY) {
		var clickedCell = this.getCell(newVec2(posX, posY));
		
		for (var i=0; i < this.binders.length; ++i) {
			var fl = this.binders[i].forkliftObject;
			if (fl.depotCell == clickedCell) {
				return fl;
			}
		}
		return null;
	}
	
	this.getShippingContainerAtCoordinate = function(posX, posY) {
		var clickedCell = this.getCell(newVec2(posX, posY));
		for (var i=0; i < this.containers.length; ++i) {
			if (this.containers[i].initialCell == clickedCell) {
				return this.containers[i];
			}
		}
		return null;
	}
	
	
	this.deleteContainer = function(container) {
		if (container == null) {
			return;
		}
		for (var i=0; i < this.containers.length; ++i) {
			if (this.containers[i] == container) {
				this.containers.splice(i, 1);
				return;
			}
		}
	}
	
	this.deleteForklift = function(forklift) {
		if (forklift == null) {
			return;
		}
		for (var i=0; i < this.binders.length; ++i) {
			if (this.binders[i].forkliftObject == forklift) {
				this.binders.splice(i, 1);
				return;
			}
		}
	}
	
	
	this.checkPathIntersections = function(driver) {
		if (driver.isInMotion() == false) {
			return null;
		}
		var d1line = driver.getCurrentRouteLineSegment();
		if (d1line == null) {
			return null;
		}
		
		for (var i=0; i < this.binders.length; ++i) {
			var driver2 = this.binders[i].driverObject;
			if (driver2 != driver) {
				var d2line = driver2.getCurrentRouteLineSegment();
				if (d2line != null) {
					if (checkLineIntersection(d1line[0], d1line[1], d2line[0], d2line[1])) {
						console.log('l1: (%d,%d) - (%d,%d),   l2: (%d,%d) - (%d,%d)',
									d1line[0], d1line[1], d2line[0], d2line[1]);
						return driver2;
					}
				}
			}
		}
		return null;
	}
	
    // Create cells
    this.createCells();
    this.markObstacles();
}


