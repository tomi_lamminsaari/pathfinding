# README #

Pathfinder Toy is an example implementation of A\* pathfinding algorithm. There are few automated forklifts which move boxes around a yard.
It is a toy in a sense that you can't do anything clever with it but it shows how A\* path finding algorithm works. It has been written with
HTML canvas, Javascript and jQuery.

You can find the online version here: https://tomilamminsaari.z16.web.core.windows.net/pathfinder/index.html

![Screeshot of the pathfinder toy.](https://bitbucket.org/tomi_lamminsaari/pathfinding/raw/95d11ed84fa44c2ed028df28433d73b1cf6b1ce5/screenshot.png)